# SiLA 2 Core Features

These are the current SiLA 2 Core features

[AuthenticationService v1.0](feature_definitions/org/silastandard/core/AuthenticationService-v1_0.sila.xml)

[AuthorizationConfigurationService v1.0](feature_definitions/org/silastandard/core/AuthorizationConfigurationService-v1_0.sila.xml)

[AuthorizationProviderService v1.0](feature_definitions/org/silastandard/core/AuthorizationProviderService-v1_0.sila.xml)

[AuthorizationService v1.0](feature_definitions/org/silastandard/core/AuthorizationService-v1_0.sila.xml)

[CancelController v1.0](feature_definitions/org/silastandard/core/commands/CancelController-v1_0.sila.xml)

[ConnectionConfigurationService v1.0](feature_definitions/org/silastandard/core/ConnectionConfigurationService-v1_0.sila.xml)

[ErrorRecoveryService v1.0](feature_definitions/org/silastandard/core/ErrorRecoveryService-v1_0.sila.xml)

[LockController v1.0](feature_definitions/org/silastandard/core/LockController-v1_0.sila.xml)

[ParameterConstraintsProvider v1.0](feature_definitions/org/silastandard/core/commands/ParameterConstraintsProvider-v1_0.sila.xml)

[PauseController v1.0](feature_definitions/org/silastandard/core/commands/PauseController-v1_0.sila.xml)

[SiLAService v1.0](feature_definitions/org/silastandard/core/SiLAService-v1_0.sila.xml)

[SimulationController v1.0](feature_definitions/org/silastandard/core/SimulationController-v1_0.sila.xml)
