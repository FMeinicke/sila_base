# SiLA 2 Standard 

## News

* **2021-05-10:** Development of a new SiLA reference implementation in JavaScript started
* **2021-04-29:** Development of a SiLA Edge Gateway started


## Introduction
SiLA’s mission is to establish international standards which create open connectivity in lab automation.
SiLA’s vision is to create interoperability, flexibility and resource optimization for laboratory instruments integration and software services based on standardized communication protocols and content specifications.
SiLA promotes open standards to allow integration and exchange of intelligent systems in a cost-effective way.

The SiLA 2 specification is a multipart specification, and the work-in-progress documents can be accessed on Google Drive:

* [Part (A) - Overview, Concepts and Core Specification](https://docs.google.com/document/d/1nGGEwbx45ZpKeKYH18VnNysREbr1EXH6FqlCo03yASM/edit)
* [Part (B) - Mapping Specification](https://docs.google.com/document/d/1-shgqdYW4sgYIb5vWZ8xTwCUO_bqE13oBEX8rYY_SJA/edit)
* [Part (C) - Features Index](https://docs.google.com/document/d/1J9gypD6HofLQZ8cPgLWljRuO0V8l5dS22TWQxFy4bhY/edit)

For more information, visit our [website](http://sila-standard.com/).

In case of general questions, contact either Carmen, our secretary ([carmen.condrau@sila-standard.org](mailto:carmen.condrau@sila-standard.org)) or
Daniel, CTO of SiLA ([daniel.juchli@sila-standard.org](mailto:daniel.juchli@sila-standard.org)).


## Organisation
Everyone is open to contributing, we organise code issues with GitLab and have weekly meetings of the
SiLA 2 Working group that is accessible to members. There is a public Slack channel that anyone can join to ask for help and/or give feedback: [slack link](https://join.slack.com/t/sila-standard/shared_invite/enQtNDI0ODcxMDg5NzkzLTBhOTU3N2I0NTc4NDcyMjg2ZDIwZDc1Yjg4N2FmYjZkMzljZDAyZjAwNTc5OTVjYjIwZWJjYjA0YTY0NTFiNDA).

For any issues regarding the reference implementations and features, please select the appropriate project and add issues accordingly [public link](https://gitlab.com/groups/SiLA2/-/issues).

## License
All work on GitLab is licensed under the [MIT License](https://en.wikipedia.org/wiki/MIT_License)
