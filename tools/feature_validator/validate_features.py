import glob
import os
import shutil

from lxml import etree
from lxml.etree import Element

ROOT_DIR = os.path.abspath(os.path.join(os.path.dirname(__file__), "..", ".."))
XSI = "http://www.w3.org/2001/XMLSchema-instance"
VALID_SILA_VERSIONS = [
    "1.0",
    "1.1",
]
SILA_SCHEMA_LOC = "https://gitlab.com/SiLA2/sila_base/raw/master/schema/FeatureDefinition.xsd"
FDL_VALIDATOR = etree.XSLT(etree.parse(os.path.join(ROOT_DIR, "xslt", "fdl-validation.xsl")))


def __xpath_with_sila_namespace(xml_node: Element, expression: str):
    return xml_node.xpath(expression, namespaces=dict(sila="http://www.sila-standard.org"))


def validate_feature(actual_filename):
    # Schema validation
    schema_xsd = etree.XMLSchema(etree.parse(os.path.join(ROOT_DIR, "schema", "FeatureDefinition.xsd")))
    feature_xml = etree.parse(actual_filename)
    schema_xsd.assertValid(feature_xml)

    # SiLA Version Check
    sila_version = feature_xml.xpath("@SiLA2Version")[0]
    if sila_version not in VALID_SILA_VERSIONS:
        raise Exception(f"SiLA2Version attribute of features in sila_base must be in {VALID_SILA_VERSIONS}")

    # Check file location
    originator_path = convert_to_path(feature_xml.xpath("@Originator"))
    category_path = convert_to_path(feature_xml.xpath("@Category"))
    schema_locations = " ".join(feature_xml.xpath("//*/@xsi:schemaLocation", namespaces={"xsi": XSI}))

    if SILA_SCHEMA_LOC not in schema_locations:
        raise Exception(
            f"Features schema location must contain '{SILA_SCHEMA_LOC}', "
            f"was '{schema_locations}' in feature defintion `{actual_filename}`."
        )

    expected_directory = os.path.join(ROOT_DIR, "feature_definitions", originator_path, category_path)

    identifier = __xpath_with_sila_namespace(feature_xml, "/sila:Feature/sila:Identifier/text()")[0]
    major_version, minor_version = __xpath_with_sila_namespace(feature_xml, "@FeatureVersion")[0].split(".")

    expected_filename = os.path.join(expected_directory, f"{identifier}-v{major_version}_{minor_version}.sila.xml")

    if expected_filename != actual_filename:
        raise Exception(f"Feature '{identifier}' in '{actual_filename}' should be at '{expected_filename}'")

    # FDL validation
    FDL_VALIDATOR(feature_xml)


def convert_to_path(feature_namespace):
    if feature_namespace:
        namespace = str(feature_namespace[0])
        return namespace.replace(".", os.sep)
    else:
        return ""


def main() -> int:
    fails = 0
    for filepath in glob.glob(os.path.join(ROOT_DIR, "feature_definitions", "**/*"), recursive=True):
        try:
            if os.path.isdir(filepath) or filepath.endswith("README.md"):
                continue
            if not filepath.endswith(".sila.xml"):
                raise Exception(f"Only feature definitions ending with '.sila.xml' are allowed! Found '{filepath}'")
            validate_feature(filepath)
        except Exception as e:
            print(e)
            fails += 1
    return fails


if __name__ == "__main__":
    exit(main())
